# Get directory where this file resides
include xil_vars.mk

INC_DIRS := $(BSP_INC_DIR) $(APP_INCLUDE_DIRS)
LIB_DIRS := $(BSP_LIB_DIR)

INC_DIR_FLAGS := $(addprefix -I,$(INC_DIRS))
LIB_DIR_FLAGS := $(addprefix -L,$(LIB_DIRS))

ELF := test.elf
ELFSIZE := $(ELF).size
ELFCHECK := $(ELF).elfcheck

###########################################################################

# All Target
all: $(ELF)

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(strip $(C_DEPS)),)
-include $(C_DEPS)
endif
endif

.PHONY: print_vars
print_vars:
	@echo "MAKEABLE_LIBRARY_ROOT_DIRS=$(MAKEABLE_LIBRARY_ROOT_DIRS)"
	@echo "LIBS=$(LIBS)"
	@echo "LD_FLAGS=$(LD_FLAGS)"
	@echo "INC_DIRS=$(INC_DIRS)"
	@echo "INC_DIR_FLAGS=$(INC_DIR_FLAGS)"
	@echo "LIB_DIRS=$(LIB_DIRS)"
	@echo "LIB_DIR_FLAGS=$(LIB_DIR_FLAGS)"
	@echo "OBJ_DIR=$(OBJ_DIR)"
	@echo "OBJS=$(OBJS)"
	@echo "C_DEPS=$(C_DEPS)"
	@echo "HW_PLATFORM_XML=$(HW_PLATFORM_XML)"
	@echo "PROCESSOR_TYPE=$(PROCESSOR_TYPE)"
	@echo "PROCESSOR_INSTANCE=$(PROCESSOR_INSTANCE)"
	@echo "CPU_FLAGS=$(CPU_FLAGS)"
	@echo "C_SRCS=$(C_SRCS)"

hello_world.o: hello_world.c
	@echo Building file: $<
	@echo Invoking: gcc compiler
	@mkdir -p $(@D)
	$(CC) $(CC_FLAGS) $(OPT_FLAGS) $(INC_DIR_FLAGS) $(CPU_FLAGS) \
	    -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo Finished building: $<
	@echo ' '

$(ELF): $(OBJS) $(LD_SCRIPT)
	@echo Building target: $@
	@echo Invoking: gcc linker
	$(CC) $(LD_FLAGS) $(LIB_DIR_FLAGS) $(CPU_FLAGS) -o"$@" $(OBJS) $(LIBS)
	@echo Finished building target: $@
	@echo ' '

$(ELFSIZE): $(ELF)
	@echo Invoking: Print Size
	$(SZ) "$<" | tee "$@"
	@echo Finished building: $@
	@echo ' '

$(ELFCHECK): $(ELF)
	@echo Invoking: Xilinx ELF Check
	elfcheck "$<" -hw $(HW_PLATFORM_XML) -pe $(PROCESSOR_INSTANCE) | tee "$@"
	@echo Finished building: $@
	@echo ' '

# Load FPGA bitstream
.PHONY: pgm
pgm:
	cd $(PROJ_ROOT) && xmd -tcl xmd_init.tcl

# Download ELF and execute
.PHONY: run
run:
	xmd -tcl $(MAKEFILE_DIR)/xmd_arm.tcl

# Other Targets
clean:
	-rm -rf $(OBJS) $(C_DEPS) $(ELFSIZE) $(ELFCHECK) $(ELF)
	-rm -rf $(OBJ_ROOT_DIR)
	-@echo ' '

.PHONY: all clean
.SECONDARY:

###########################################################################
.PHONY : clean_all
clean_all : clean 

###########################################################################

