The source code files are modified from example codes provided by Mohammad S Sadri, modified by Han.

The list of source files:
1. `ps_dma_seq.c` 			PS Sequential Read/Write Test	
2. `ps_dma_rand.c`        	PS Random Read/Write Test 

The run*.sh scripts are responsible to run the cross-compilation, loading the .bin to Zedboard and execute. 


