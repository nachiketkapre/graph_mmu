#!/usr/bin/zsh

export PATH=$PATH:/opt/Xilinx/SDK/2013.4/gnu/arm/lin/bin/
export PATH=$PATH:/opt/Xilinx/SDK/2013.2/gnu/arm/lin/bin/
arm-xilinx-eabi-gcc -std=c99 -I../SDK/hw -I../bsp/ps7_cortexa9_0/include -c -o platform.o platform.c
arm-xilinx-eabi-gcc -std=c99 -I../SDK/hw -I../bsp/ps7_cortexa9_0/include -c -o ps_dma_rand.o ps_dma_rand.c
arm-xilinx-eabi-gcc -Wl,-T -Wl,../etc/lscript.ld -L../bsp/ps7_cortexa9_0/lib -o test.elf ps_dma_rand.o platform.o -Wl,--start-group,-lxil,-lgcc,-lc,-lm,--end-group

export PATH=$PATH:/opt/Xilinx/SDK/2013.4/bin/lin64
pushd ../; xmd -tcl xmd_init.tcl; popd
xmd -tcl xmd_arm.tcl

tail -50 ~/session.log
