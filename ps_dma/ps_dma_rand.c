#include <stdlib.h>
#include "platform.h"
#include "hardware_timer.h"
#include "ps7_init.h"
#include <xil_io.h>
#include "xparameters.h"

#define PREFETCH_CTRL_REG 0xF8F02F60
#define DISABLE_PREFETCH 1 

// Interrupt controller
unsigned int PACKET_SIZE = 1;

int rand_seed[10] = {197, 409, 467, 557, 617, 683, 761, 839, 919,997};

int main()
{
	init_platform();

#ifdef DISABLE_PREFETCH
    u32 reg_val;
    reg_val = Xil_In32(PREFETCH_CTRL_REG);
    reg_val = reg_val & 0x00000000;
    Xil_Out32(PREFETCH_CTRL_REG, reg_val);
    reg_val = Xil_In32(PREFETCH_CTRL_REG);
    xil_printf("Prefetching disabled. PREFETCH_CTRL_REG value = %08x\n\r", reg_val);
#endif

	xil_printf("Starting PS-only Random write tests\n\r");
	
	int* a = (int*)malloc(sizeof(int)*1024);
	int b, c, d;
	// memory write
	int rand_addr[32768*1024];
	for(int i=0;i<=12;i++) {
	   	for(int s=0;s<4096;s++){
		     srand(rand_seed[s % 10]);
		     rand_addr[s]=rand() % 64;
		}

		init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		start_timer(timer_ctrl);
		for(int j=0;j<PACKET_SIZE;j++) {
			for(int k=0;k<10;k++){
				a[rand_addr[j]]=j;
			}
		}
		stop_timer(timer_ctrl);
		xil_printf("PS-Writes, packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/3330);
		PACKET_SIZE=PACKET_SIZE*2;
	}
	// memory read
 	PACKET_SIZE = 32768*1024;
	xil_printf("PS-only Random read tests\n\r");

	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
	start_timer(timer_ctrl);
	for(int k=0;k<10;k++){
			for(int j=0;j<PACKET_SIZE;j++) {
					d = a[rand_addr[j]];
			}
	}
	stop_timer(timer_ctrl);
	xil_printf("PS-Reads, packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/3330);

	xil_printf("All PS transfers are over?\n\t");

	return 0;
}
