#include <stdlib.h>
#include "platform.h"
#include "hardware_timer.h"
#include "ps7_init.h"
#include <xil_io.h>
#include "xparameters.h"

#define PREFETCH_CTRL_REG 0xF8F02F60
#define L2_CACHE_CTRL_REG 0xF8F02100
#define DISABLE_PREFETCH 0 
#define DISABLE_CACHE 1
unsigned int PACKET_SIZE = 1;


int main()
{
	init_platform();
#ifdef DISABLE_PREFETCH
	u32 reg_val;
	reg_val = Xil_In32(PREFETCH_CTRL_REG);
	reg_val = reg_val & 0x00000000;
	Xil_Out32(PREFETCH_CTRL_REG, reg_val);

	Xil_Out32(0xF8F02104, 0x02060000);
	reg_val = Xil_In32(PREFETCH_CTRL_REG);
	xil_printf("Prefetching disabled. PREFETCH_CTRL_REG value = %08\n\r", reg_val);
#endif

#ifdef DISABLE_CACHE
	Xil_Out32(L2_CACHE_CTRL_REG, 0x0);
#endif

	xil_printf("Starting PS-only tests\n\r");

	int* a = (int*)malloc(sizeof(int)*32);
	int b, c, d;
	// memory write
	for(int i=0;i<=12;i++) {
		init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		start_timer(timer_ctrl);
		for(int j=0;j<PACKET_SIZE;j++) {
			a[j]=j;
		}

		stop_timer(timer_ctrl);
		xil_printf("PS-Writes, packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/333);
		PACKET_SIZE=PACKET_SIZE*2;
	}
	// memory read
 	PACKET_SIZE = 1;
	xil_printf("PS-only read tests\n\r");
	for(int i=0;i<=12;i++) {
		init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		start_timer(timer_ctrl);
		for(int j=0;j<PACKET_SIZE;j++) {
			d = a[j];
		}
		stop_timer(timer_ctrl);
		xil_printf("PS-Reads, packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/333);
		PACKET_SIZE=PACKET_SIZE*2;
	}
		
	xil_printf("All PS transfers are over?\n\t");

	return 0;
}
