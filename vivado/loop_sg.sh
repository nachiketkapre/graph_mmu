#!/bin/sh
for var in 2 4 8 16 32 64 128 256
do
	sed -ri "s/(burst_size )\{[0-9]+\}/\1{$var}/g" src/bd/design_1_sg.tcl
	./build_sg.sh
    cp -v axi_dma_sg/axi_dma_sg.runs/impl_1/design_1_wrapper.bit bit/sg_$var/
done
