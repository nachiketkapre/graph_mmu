#!/bin/sh
for var in 2 4 8 16 32 64 128 256
do
	sed -ri "s/(burst_size )\{[0-9]+\}/\1{$var}/g" src/bd/design_1_reg.tcl
	./build_reg.sh
    cp -v axi_dma_reg/axi_dma_reg.runs/impl_1/design_1_wrapper.bit bit/reg_$var/
done
