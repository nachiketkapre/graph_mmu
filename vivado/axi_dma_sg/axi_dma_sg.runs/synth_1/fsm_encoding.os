
 add_fsm_encoding \
       {rd_chnl.rlast_sm_cs} \
       { }  \
       {{000 000} {001 100} {010 010} {011 001} {100 011} }

 add_fsm_encoding \
       {rd_chnl__parameterized0.rlast_sm_cs} \
       { }  \
       {{000 000} {001 100} {010 010} {011 001} {100 011} }

 add_fsm_encoding \
       {axi_dma_mm2s_sm.mm2s_cs} \
       { }  \
       {{00 000} {01 001} {10 011} {11 010} }

 add_fsm_encoding \
       {axi_dma_s2mm_sm.GEN_SM_FOR_NO_LENGTH.s2mm_cs} \
       { }  \
       {{00 000} {01 001} {10 011} {11 010} }

 add_fsm_encoding \
       {axi_data_fifo_v2_1_axic_reg_srl_fifo.state} \
       { }  \
       {{00 010} {01 011} {10 000} {11 001} }

 add_fsm_encoding \
       {axi_data_fifo_v2_1_axic_reg_srl_fifo__parameterized0.state} \
       { }  \
       {{00 010} {01 011} {10 000} {11 001} }

 add_fsm_encoding \
       {axi_data_fifo_v2_1_axic_reg_srl_fifo__parameterized1.state} \
       { }  \
       {{00 010} {01 011} {10 000} {11 001} }

 add_fsm_encoding \
       {axi_data_fifo_v2_1_axic_reg_srl_fifo__parameterized2.state} \
       { }  \
       {{00 010} {01 011} {10 000} {11 001} }
