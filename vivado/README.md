This directory contains scripts to build Vivado project from tcl file and generate bitstreams
1. Execute ./build_[reg, sg].sh to generate bitstreams for register mode and scatter gather mode respectively
2. Block diagram information is in src/bd/design_1.tcl
3. To change AXI DMA setting, change the environment variable in ./build.sh
