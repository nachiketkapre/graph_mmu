
/*******************************************************************************
* LLFIFO component and parameters
*******************************************************************************/

Folder FOLDER_xtag_csp_LLFIFO {
  NAME       LLFIFO
  SYNOPSIS   LLFIFO support
  _CHILDREN  FOLDER_xtag_csp_CSP
}

Component INCLUDE_XLLFIFO {
  NAME       LLFIFO interface
  SYNOPSIS   LLFIFO interface
  REQUIRES   
  _CHILDREN  FOLDER_xtag_csp_LLFIFO
}

