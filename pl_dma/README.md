The source code files are modified from [examples](http://www.googoolia.com/wp/category/zynq-training/) provided by Mohammad S Sadri, modified by Han.

The list of source files:
1. `pl_dma.c` 			DMA in PL Write Test (forgot which, maybe the same as 3)		
2. `pl_dma_r.c`        	DMA in PL Read Test (forgot which, maybe the same as 4)
3. `dma_reg_write.c`	AXO_DMA in Register Mode Write (S2MM)
4. `dma_reg_read.c`		AXI_DMA in Register Mode Read (MM2S)
5. `dma_sg_write.c`		AXI_DMA	in Scatter Gather Mode Write (S2MM) *Polling*
6. `dma_sg_read.c`		AXI_DMA in Scatter Gather Mode Read (MM2S)	*Interrupt*

The run*.sh scripts are responsible to run the cross-compilation, loading the .bin to Zedboard and execute. 


