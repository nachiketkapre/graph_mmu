// Sadri -  29 - aug - 2014 - created!

// Author : Mohammad S Sadri - Example design for AXI DMA scatter-gather mode of operation

// This code is distributed under BSD license.

#include <stdio.h>
#include "platform.h"
#include "ps7_init.h"
#include <xil_io.h>
#include "xscugic.h"
#include "xparameters.h"
#include "hardware_timer.h"
#include <stdlib.h>

#define RAND 0

#define DESTINATION_ADDRESS_IN_DRAM 0x0a000000

#define MM2S_DMACR	0x00
#define MM2S_DMASR	0x04
#define MM2S_CURDESC	0x08
#define MM2S_TAILDESC	0x10

#define S2MM_DMACR	0x30
#define S2MM_DMASR	0x34
#define S2MM_CURDESC	0x38
#define S2MM_TAILDESC	0x40
#define ALIGN 0x40

#define NXTDESC		0x00
#define BUFFER_ADDRESS	0x08
#define CONTROL		0x18
#define STATUS		0x1c
// Interrupt controller
XScuGic InterruptController;
static XScuGic_Config *GicConfig;
u32 global_frame_counter = 0;

static int TOTAL_NUMBER_OF_DESCRIPTORS = 0;
static int DEFAULT_LENGTH = 128;
static int TOTAL_TRANSFER_LENGTH = 0;

// sg descriptor structure
// size of each descriptor is : 13 x 4 = 52 bytes
// important point is some fields are reserved.
struct sg_descriptor {
	u32	nextDesc;
	u32 bufferAddress;
	u32 control;
	u32 status;
//	u32 app[5];
};

// we create a variable with sg_descriptor type

void InterruptHandler ( void ) {
	// if you have a device, which may produce several interrupts one after another, the first thing you should do is to disable interrupts. but axi dma is not this case.
	stop_timer(timer_ctrl);
	xil_printf("For %d descriptors and %d bytes, Scatter-Gather DMA required %d us\n\r", TOTAL_NUMBER_OF_DESCRIPTORS, TOTAL_TRANSFER_LENGTH, (*timer_counter_l)/333);
	int tmpValue=0;
	// clear interrupt. just perform a write to bit no. 12 of S2MM_DMASR
	tmpValue = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR );
	tmpValue =tmpValue | 0x1000;
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR , tmpValue );

	TOTAL_NUMBER_OF_DESCRIPTORS=TOTAL_NUMBER_OF_DESCRIPTORS*2;
	global_frame_counter=1;

}

//////////////////////////////////////////////////////////////////
//
// Setup and Initialize Interrupt System
//
//////////////////////////////////////////////////////////////////

int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, XScuGicInstancePtr);
	Xil_ExceptionEnable();		// enable interrupts in ARM.
	return XST_SUCCESS;
}

int InitializeInterruptSystem  ( deviceID ) {
	int Status;

	GicConfig = XScuGic_LookupConfig ( deviceID );
	if ( NULL == GicConfig ) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize ( &InterruptController, GicConfig, GicConfig->CpuBaseAddress);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = SetUpInterruptSystem ( &InterruptController);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = XScuGic_Connect ( &InterruptController,
			XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR,
			(Xil_ExceptionHandler)InterruptHandler,
			NULL);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_Enable (&InterruptController, XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR);

	return XST_SUCCESS;
}

//////////////////////////////////////////////////////////////////
//
// Enable Sample Generator
//
//////////////////////////////////////////////////////////////////
int EnableSampleGenerator ( unsigned int numberOfWords ) {

	// set the value for FrameSize
	Xil_Out32 ( XPAR_AXI_GPIO_0_BASEADDR , numberOfWords );

	// enable the SampleGenerator
	Xil_Out32 ( XPAR_AXI_GPIO_1_BASEADDR , 1 );

	return 0;
}

//////////////////////////////////////////////////////////////////
//
// Initialize AXI DMA
//
//////////////////////////////////////////////////////////////////
int InitializeAXIDma ()  {

	int tmpVal;

	Xil_Out32  ( XPAR_AXI_DMA_0_BASEADDR + S2MM_CURDESC, XPAR_BRAM_0_BASEADDR );

	Xil_Out32 (XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR, 0x0000000);
	
	// S2MM_DMACR.RS = 1
	tmpVal = 0;
	xil_printf ( "value for dma control register before change: %08x\n\r", tmpVal);
	// dma unit enable, interrupt on complete enable, interrupt on error enabled

	tmpVal = tmpVal | 0x5001; // cyclic mode inactive

	Xil_Out32  ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR, tmpVal );
	
	int tmpVal2;
	tmpVal2 = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR);
	xil_printf ( "value for dma control register after change: %08x\n\r", tmpVal2 );

	return 0;
}

//////////////////////////////////////////////////////////////////
//
// Start DMA Transfer
//
//////////////////////////////////////////////////////////////////

void StartDMATransfer ( int dstAddress, int len) {
		Xil_Out32  ( XPAR_AXI_DMA_0_BASEADDR + S2MM_TAILDESC, XPAR_BRAM_0_BASEADDR + (TOTAL_NUMBER_OF_DESCRIPTORS-1) * ALIGN);
}

//////////////////////////////////////////////////////////////////
//
// Initialize descriptors
//
//////////////////////////////////////////////////////////////////

void InitializeDescriptors ( int dstAddress, int TOTAL_NUMBER_OF_DESCRIPTORS ) {

	struct sg_descriptor s2mmDescriptors[TOTAL_NUMBER_OF_DESCRIPTORS];
	int i;

	int size;
	int address = dstAddress;

	for ( i=0; i<TOTAL_NUMBER_OF_DESCRIPTORS; i++ )  {
		if ( i == (TOTAL_NUMBER_OF_DESCRIPTORS-1) )	
			s2mmDescriptors[i].nextDesc = 0;
		else
			s2mmDescriptors[i].nextDesc =  XPAR_BRAM_0_BASEADDR + (i+1) * ALIGN;

#if RAND
		size =  rand() % 128 + 1;
#else
		size = DEFAULT_LENGTH
#endif

		s2mmDescriptors[i].bufferAddress = address;
		s2mmDescriptors[i].control = size;
		TOTAL_TRANSFER_LENGTH += size;
		address += size;

		if ( i == 0 )
			s2mmDescriptors[i].control |= 0x08000000;
		if ( i == (TOTAL_NUMBER_OF_DESCRIPTORS-1) )
			s2mmDescriptors[i].control |= 0x04000000;
	}

	// Copy the descriptors data to BRAM
	for ( i=0; i < TOTAL_NUMBER_OF_DESCRIPTORS; i++ )  {
		Xil_Out32 ( XPAR_BRAM_0_BASEADDR + i*ALIGN + NXTDESC       , s2mmDescriptors[i].nextDesc );
		Xil_Out32 ( XPAR_BRAM_0_BASEADDR + i*ALIGN + BUFFER_ADDRESS  , s2mmDescriptors[i].bufferAddress );
		Xil_Out32 ( XPAR_BRAM_0_BASEADDR + i*ALIGN + CONTROL , s2mmDescriptors[i].control );
		Xil_Out32 ( XPAR_BRAM_0_BASEADDR + i*ALIGN + STATUS , 0 );
	}
 }

void readDesc (int addr){
	int tmpVal;
	tmpVal = Xil_In32 ( addr + NXTDESC );
	xil_printf("Next descript: %08x\n\r", tmpVal );
	tmpVal = Xil_In32 ( addr + BUFFER_ADDRESS);
    xil_printf("Buffer addr: %08x\n\r", tmpVal );
	tmpVal = Xil_In32 ( addr + CONTROL);
    xil_printf("Control register: %08x\n\r", tmpVal );
    tmpVal = Xil_In32 ( addr + STATUS);
    xil_printf("Status register: %08x\n\r", tmpVal );
}

void readDMAReg(void){
	int tmpVal;
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR );
	xil_printf("S2MM control register: %08x\n\r", tmpVal);
    tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR);
	xil_printf("S2MM status register: %08x\n\r", tmpVal);
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_CURDESC);
	xil_printf("S2MM Current Desc register: %08x\n\r", tmpVal);
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_TAILDESC);
	xil_printf("S2MM Tail Desc register: %08x\n\r", tmpVal);
}

//////////////////////////////////////////////////////////////////
//
// Main
//
//////////////////////////////////////////////////////////////////
int main()
{
    init_platform();
    InitializeAXIDma ();

    // set the interrupt system and interrupt handling interrupt
	xil_printf ("enabling the interrupt handling system...\n\r");
	InitializeInterruptSystem ( XPAR_PS7_SCUGIC_0_DEVICE_ID );

	xil_printf ("Performing the first dma transfer....\n\r");

	u32 tmpValue;
	tmpValue = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR );
	xil_printf( "Initial DMA status register value: %08x \n\r", tmpValue );

	TOTAL_NUMBER_OF_DESCRIPTORS=2;
	for(int i=0;i<10;i++) {
			// Trigger the DMA engine
			init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
			start_timer(timer_ctrl);

			InitializeDescriptors ( DESTINATION_ADDRESS_IN_DRAM, TOTAL_NUMBER_OF_DESCRIPTORS);
			stop_timer(timer_ctrl);
			xil_printf("Initialization of decsriptor chain of length %d took %dus\n\r", TOTAL_NUMBER_OF_DESCRIPTORS, (*timer_counter_l)/333);

			EnableSampleGenerator ( TOTAL_TRANSFER_LENGTH - 1);
			
			init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
			start_timer(timer_ctrl);
			StartDMATransfer ( (int)DESTINATION_ADDRESS_IN_DRAM, (int) TOTAL_TRANSFER_LENGTH);
			// busy wait for interrupt to arrive
			u32 tmpValue;
			while ( global_frame_counter == 0 )  {
				tmpValue = Xil_In32 (  XPAR_BRAM_0_BASEADDR + (TOTAL_NUMBER_OF_DESCRIPTORS -1) * ALIGN + STATUS );
				global_frame_counter = tmpValue & 0x80000000;
			}
				
			stop_timer(timer_ctrl);
			xil_printf("For %d descriptors and %d bytes, Scatter-Gather DMA required %d us\n\r", TOTAL_NUMBER_OF_DESCRIPTORS, TOTAL_TRANSFER_LENGTH, (*timer_counter_l)/333);
			TOTAL_NUMBER_OF_DESCRIPTORS= TOTAL_NUMBER_OF_DESCRIPTORS * 2;
			global_frame_counter=0;
			TOTAL_TRANSFER_LENGTH=0;
			xil_printf( "===================================================== \n\r");
		

	}

    return 0;
}
