#!/usr/bin/zsh

# setup paths
export PATH=$PATH:/opt/Xilinx/SDK/2013.4/gnu/arm/lin/bin/
export PATH=$PATH:/opt/Xilinx/SDK/2013.4/bin/lin64

# cross-compile the C code with arm-gcc
arm-xilinx-eabi-gcc -std=c99 -I../../axi_dma/SDK/hw -I../../axi_dma/bsp/ps7_cortexa9_0/include -c -o platform.o platform.c
arm-xilinx-eabi-gcc -std=c99 -I../../axi_dma/SDK/hw -I../../axi_dma/bsp/ps7_cortexa9_0/include -c -o dma_reg_read.o dma_reg_read.c
arm-xilinx-eabi-gcc -Wl,-T -Wl,../../axi_dma/etc/lscript.ld -L../../axi_dma/bsp/ps7_cortexa9_0/lib -o test.elf dma_reg_read.o platform.o -Wl,--start-group,-lxil,-lgcc,-lc,-lm,--end-group

# load FPGA bitstream and execute code
cp -v ../vivado/bit/reg_32/design_1_wrapper.bit ../SDK/hw/system.bit
pushd ../../axi_dma; xmd -tcl xmd_init.tcl; popd
xmd -tcl xmd_arm.tcl

tail -n 30 ~/session.log
