SDK_EXPORT_ROOT := $(PROJ_ROOT)/SDK
HW_PLATFORM_XML := $(SDK_EXPORT_ROOT)/hw/system.xml
BSP_ROOT_DIR := $(PROJ_ROOT)/bsp

# CPU gcc flags
# e.g. for MicroBlaze:
# CPU_FLAGS := -mlittle-endian -mxl-barrel-shift -mxl-pattern-compare \
#              -mcpu=v8.40.b -mno-xl-soft-mul
CPU_FLAGS :=
# e.g. -DUSE_ZYNQ_UART for MicroBlaze on Zynq.
CMACRO_DEFS :=
# microblaze, cortexa9
PROCESSOR_TYPE := cortexa9 
# e.g. microblaze_0, ps7_cortexa9_0
PROCESSOR_INSTANCE := ps7_cortexa9_0

OPT_FLAGS := -O3
# Debug opt flags:
# OPT_FLAGS := -O0 -g3 -Wl,--no-relax
CC_FLAGS := -Wall -c -fmessage-length=0 $(CMACRO_DEFS) $(EXTRA_CC_FLAGS)

BSP_INC_DIR  := $(BSP_ROOT_DIR)/$(PROCESSOR_INSTANCE)/include
BSP_LIB_DIR  := $(BSP_ROOT_DIR)/$(PROCESSOR_INSTANCE)/lib

CC := arm-xilinx-eabi-gcc
AR := arm-xilinx-eabi-ar
SZ := arm-xilinx-eabi-size

# LD_SCRIPT := lscript.ld
LD_SCRIPT := $(PROJ_ROOT)/etc/lscript.ld

LD_FLAGS := -Wl,-T -Wl,$(LD_SCRIPT)
# Debug linker flags
# LD_FLAGS += -Wl,--no-relax

LIBS += -Wl,--start-group,-lxil,-lgcc,-lc,-lm,--end-group

OBJS := $(C_SRCS:.c=.o)
C_DEPS := $(OBJS:.o=.d)

