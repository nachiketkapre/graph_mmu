#include <stdlib.h>
#include "platform.h"
#include "hardware_timer.h"
#include "ps7_init.h"
#include <xil_io.h>
#include "xscugic.h"
#include "xparameters.h"

// Interrupt controller
XScuGic InterruptController;
static XScuGic_Config *GicConfig;
unsigned int global_frame_counter = 0;
unsigned int PACKET_SIZE = 16;


void InterruptHandler ( void ) {
	u32 tmpValue;
	stop_timer(timer_ctrl);
	xil_printf("PL-based DMA Read, Packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/333);

	tmpValue = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + 0x04 );
	tmpValue = tmpValue | 0x1000;
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x04 , tmpValue );

	global_frame_counter++;
	if(global_frame_counter>=19) {
		return;
	}

	// initiate a new data transfer
	PACKET_SIZE=PACKET_SIZE*2;
	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);	
	start_timer(timer_ctrl);
	StartDMATransfer_Read ( 0xa000000, PACKET_SIZE );
}

int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, XScuGicInstancePtr);
	Xil_ExceptionEnable();		// enable interrupts in ARM.
	return XST_SUCCESS;
}

int InitializeInterruptSystem ( deviceID ) {
	int Status;

	GicConfig = XScuGic_LookupConfig ( deviceID );
	if ( NULL == GicConfig ) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize ( &InterruptController, GicConfig, GicConfig->CpuBaseAddress);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = SetUpInterruptSystem ( &InterruptController);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	Status = XScuGic_Connect ( &InterruptController,
	XPAR_FABRIC_AXI_DMA_0_MM2S_INTROUT_INTR,
	(Xil_ExceptionHandler)InterruptHandler,
	NULL);



	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_Enable (&InterruptController, XPAR_FABRIC_AXI_DMA_0_MM2S_INTROUT_INTR);

	return XST_SUCCESS;
}


int EnableSampleGenerator ( unsigned int numberOfWords ) {
	Xil_Out32 ( XPAR_AXI_GPIO_0_BASEADDR , numberOfWords );
	// enable the SampleGenerator
	Xil_Out32 ( XPAR_AXI_GPIO_1_BASEADDR , 1 );

	return 0;
}


int InitializeAXIDma_Read ( void )  {

	unsigned int tmpVal;

	// MM2S_DMACR.RS = 1, offset 0x00
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR );
	tmpVal = tmpVal | 0x1001; // dma unit enable, interrupt on complete enable.
	Xil_Out32  ( XPAR_AXI_DMA_0_BASEADDR, tmpVal );
	return 0;
}


void StartDMATransfer ( unsigned int dstAddress, unsigned int len ) {
	// write destination address to S2MM_DA register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x48 , dstAddress );

	// write length to S2MM_LENGTH register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x58, len );
}

void StartDMATransfer_Read ( unsigned int srcAddress, unsigned int len ) {
	// write destination address to S2MM_DA register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x18 , srcAddress );

	// write length to S2MM_LENGTH register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x28, len );
}

void DMA_Read (void)
{
	xil_printf ("performing the first dma read transfer... \n\r");
	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
	start_timer(timer_ctrl);
	StartDMATransfer_Read ( 0xa000000, PACKET_SIZE );
	// busy wait for all DMAs to finish
	while ( global_frame_counter <= 20 )  {
	}

	xil_printf("All DMAs write are over? global_frame_counter=%d\n\t",global_frame_counter);
}

int main()
{
	init_platform();
	xil_printf ("initializing axi dma ...\n\r");
	InitializeAXIDma_Read ();

	// set the interrupt system and interrupt handling
	xil_printf ("enabling the interrupt handling system...\n\r");
	InitializeInterruptSystem ( XPAR_PS7_SCUGIC_0_DEVICE_ID );

	unsigned int tmpVal;
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + 0x04);
	xil_printf("Status register value: %d", tmpVal);
	DMA_Read();
	return 0;
}
