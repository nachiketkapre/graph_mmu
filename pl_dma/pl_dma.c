#include <stdlib.h>
#include "platform.h"
#include "hardware_timer.h"
#include "ps7_init.h"
#include <xil_io.h>
#include "xscugic.h"
#include "xparameters.h"

XScuGic InterruptController;
static XScuGic_Config *GicConfig;
unsigned int global_frame_counter = 0;
unsigned int PACKET_SIZE = 16;

void InterruptHandler ( void ) {
	u32 tmpValue;

	stop_timer(timer_ctrl);
	xil_printf("PL-based DMA, Packet legnth=%d, Communication time %d us\n\r", PACKET_SIZE, (*timer_counter_l)/333);

	tmpValue = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + 0x34 );
	tmpValue = tmpValue | 0x1000;
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x34 , tmpValue );

	global_frame_counter++;
	if(global_frame_counter>20) {
		return;
	}
	// initiate a new data transfer
	PACKET_SIZE=PACKET_SIZE * 2;
	EnableSampleGenerator ( PACKET_SIZE / 4 );
	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);	
	start_timer(timer_ctrl);
	StartDMATransfer ( 0xa000000, PACKET_SIZE );
}

int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, XScuGicInstancePtr);
	Xil_ExceptionEnable();		// enable interrupts in ARM.
	return XST_SUCCESS;
}

int InitializeInterruptSystem  ( deviceID ) {
	int Status;

	GicConfig = XScuGic_LookupConfig ( deviceID );
	if ( NULL == GicConfig ) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize ( &InterruptController, GicConfig, GicConfig->CpuBaseAddress);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = SetUpInterruptSystem ( &InterruptController);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = XScuGic_Connect ( &InterruptController,
			XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR,
			(Xil_ExceptionHandler)InterruptHandler,
			NULL);
	if ( Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_Enable (&InterruptController, XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR);

	return XST_SUCCESS;
}

//////////////////////////////////////////////////////////////////
//
// Enable Sample Generator
//
//////////////////////////////////////////////////////////////////
int EnableSampleGenerator ( unsigned int numberOfWords ) {
	// set the value for FrameSize
	Xil_Out32 ( XPAR_AXI_GPIO_0_BASEADDR , numberOfWords );

	// enable the SampleGenerator
	Xil_Out32 ( XPAR_AXI_GPIO_1_BASEADDR , 1 );

	return 0;
}

//////////////////////////////////////////////////////////////////
//
// Initialize AXI DMA
//
//////////////////////////////////////////////////////////////////
int InitializeAXIDma ( void )  {

	unsigned int tmpVal;
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + 0x30 );
	tmpVal = tmpVal | 0x1001;									// dma unit enable, interrupt on complete enable.
	Xil_Out32  ( XPAR_AXI_DMA_0_BASEADDR + 0x30, tmpVal );
	tmpVal = Xil_In32 ( XPAR_AXI_DMA_0_BASEADDR + 0x30 );

	return 0;
}

//////////////////////////////////////////////////////////////////
//
// Start DMA Transfer
//
//////////////////////////////////////////////////////////////////
void StartDMATransfer ( unsigned int dstAddress, unsigned int len ) {
	// write destination address to S2MM_DA register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x48 , dstAddress );

	// write length to S2MM_LENGTH register.
	Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + 0x58, len );
}

int main()
{
	init_platform();

	xil_printf ("initializing axi dma ...\n\r");
	InitializeAXIDma ();

	xil_printf ("enabling the interrupt handling system...\n\r");
	InitializeInterruptSystem ( XPAR_PS7_SCUGIC_0_DEVICE_ID );

	xil_printf ("performing the first dma transfer... \n\r");

	EnableSampleGenerator ( PACKET_SIZE / 4 );
	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
	StartDMATransfer ( 0xa000000, PACKET_SIZE);
	start_timer(timer_ctrl);

	// busy wait for all DMAs to finish
	while ( global_frame_counter < 19 )  {
	}

	xil_printf("All DMAs are over? global_frame_counter=%d\n\t",global_frame_counter);

	return 0;
}
