# Instructions

This is the git repository for the RAW 2015 paper 'GraphMMU: Memory Management Unit for Sparse Graph Accelerators'. We provide two kinds of flows to get you started with our memory management unit:

1. If you simply want to benchmark our hardware, you can take on of our pre-compile demo bitstreams and modify the C code provided in **ps_dma** and **pl_dma**. 
2. If you want to generate your own hardware and/or integrate with an existing design, look inside th **vivado** folder for bitstream generation.

# Folder Structure
You probably only need to peep inside the following folders. Altering files in other directories may cause unexpected consequences. 

1. **ps_dma** contains sources and scripts to run experiments with the PS(ARM) only. Here, the ARM CPU performs memory accesses for various access patterns. You have the option to disable cache if you so desire.
2. **pl_dma** contains sources and scripts to run experiments with the PL(FPGA). Here, the ARM CPU supplies addresses to the FPGA memory management unit. The FPGA then initiates irregular data accesses as required. 
3. **vivado** contains *tcl* scripts to generate bitstreams with specific setup. You can modify the hardware using this flow.

# Compatibility Notes
The program is tested on Ubuntu 14.04 with Vivado 2013.4 installed. The program may work on newer version of the tool, but we have not tested it. While we provide a command-line flow, less-comfortable users may be able to find a GUI-based workflow using Vivado (hardware) and Eclipse (software).

# Known Issues
1. No performance benefits are observed after increased DMA burst length to 64 and higher for both write and read. Hence we do not provide bitstreams for those cases.
2. PS write and read capped at packet length = 1024. Changing **malloc** did not have any effect beyond 1024 lengths. Increase loop iterations caused program to freeze. We did not investigate this behavior further.
3. Interrupt-based stepping is not reliably working for scatter-gather mode write operations. Hence, we use polling for completion for timing measurement (this may actually slightly undersell our performance :) ).
4. We have not investigated clock frequency improvements in this flow. With suitable constraints and floorplanning, it may be possible to make our design run faster.

The code is distributed under a BSD license. Portions of this code are dervied from Mohammad Sadri. 
